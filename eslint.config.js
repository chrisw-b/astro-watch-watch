import { astro } from "@chrisw-b/eslint-config";
export default [
  // add more generic rule sets here, such as:
  // js.configs.recommended,
  ...astro,
];
